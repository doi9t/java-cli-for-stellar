/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.handlers;

import org.stellar.sdk.AccountRequiresMemoException;
import org.stellar.sdk.Network;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.requests.*;
import org.stellar.sdk.responses.RootResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;

public interface StellarServerHandler {
    RootResponse root() throws IOException;

    AccountsRequestBuilder accounts();

    AssetsRequestBuilder assets();

    EffectsRequestBuilder effects();

    LedgersRequestBuilder ledgers();

    OffersRequestBuilder offers();

    OperationsRequestBuilder operations();

    FeeStatsRequestBuilder feeStats();

    OrderBookRequestBuilder orderBook();

    TradesRequestBuilder trades();

    PaymentsRequestBuilder payments();

    TransactionsRequestBuilder transactions();

    SubmitTransactionResponse submitTransaction(Transaction transaction) throws IOException, AccountRequiresMemoException;

    Network getNetwork();
}
