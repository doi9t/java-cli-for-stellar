/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.handlers;

import ca.watier.tools.models.ApplicationParameters;
import ca.watier.tools.models.YesNoResponse;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleIOHandlerImpl implements IOHandler {
    public static final Options OPTIONS = getOptions();
    private static final HelpFormatter HELP_FORMATTER = getFormatter();

    @Override
    public void println(String message) {
        System.out.println(message);
    }

    @Override
    public void printf(String template, Object... messages) {
        System.out.printf(template, messages);
    }

    @Override
    public void errorPrintln(String message) {
        System.err.println(message);
    }

    @Override
    public void errorPrintf(String template, Object... messages) {
        System.err.printf(template, messages);
    }

    @Override
    public void printHelp() {
        HELP_FORMATTER.printHelp("java-cli-for-stellar", OPTIONS);
    }

    @Override
    public int readNextByte() throws IOException {
        return System.in.read();
    }

    @Override
    public YesNoResponse askYesNoQuestion(String message) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(message);
        YesNoResponse response = YesNoResponse.from(scanner.nextLine());

        while (response == null) {
            System.err.println("Please, enter a valid choice [`yes`, `y`, `no` or `n`]");
            response = YesNoResponse.from(scanner.nextLine());
        }

        return response;
    }

    private static HelpFormatter getFormatter() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.setWidth(480);
        return helpFormatter;
    }

    private static Options getOptions() {
        Options options = new Options();

        for (ApplicationParameters applicationParameters : ApplicationParameters.values()) {
            int nbOfArgs = applicationParameters.getNbOfArgs();
            String longOption = applicationParameters.getLongOption();
            String shortOption = applicationParameters.getShortOption();
            String description = applicationParameters.getDescription();
            boolean hasArg = nbOfArgs > 0;

            org.apache.commons.cli.Option currentOption = new org.apache.commons.cli.Option(shortOption, longOption, hasArg, description);
            currentOption.setArgs(nbOfArgs);
            currentOption.setArgName(applicationParameters.getArgName());
            currentOption.setOptionalArg(applicationParameters.isArgumentsOptional());

            options.addOption(currentOption);
        }

        return options;
    }
}
