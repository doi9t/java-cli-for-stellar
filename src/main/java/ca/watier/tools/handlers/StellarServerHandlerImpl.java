/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.handlers;

import ca.watier.tools.models.ApplicationParameters;
import ca.watier.tools.models.StellarHorizonServer;
import ca.watier.utils.collections.MultiValueCollection;
import org.stellar.sdk.AccountRequiresMemoException;
import org.stellar.sdk.Network;
import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.requests.*;
import org.stellar.sdk.responses.RootResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;

import static ca.watier.tools.models.ApplicationParameters.TEST;

public class StellarServerHandlerImpl implements StellarServerHandler {
    private final Server server;
    private final StellarHorizonServer horizonServer;

    public StellarServerHandlerImpl(MultiValueCollection<ApplicationParameters, String> options) {
        horizonServer = getHorizonServer(options);
        server = new Server(horizonServer.getUrl());
    }

    private StellarHorizonServer getHorizonServer(MultiValueCollection<ApplicationParameters, String> parameters) {
        if (parameters.containsKey(TEST)) {
            return StellarHorizonServer.TEST;
        } else {
            return StellarHorizonServer.PUBLIC;
        }
    }

    @Override
    public RootResponse root() throws IOException {
        return server.root();
    }

    @Override
    public AccountsRequestBuilder accounts() {
        return server.accounts();
    }

    @Override
    public AssetsRequestBuilder assets() {
        return server.assets();
    }

    @Override
    public EffectsRequestBuilder effects() {
        return server.effects();
    }

    @Override
    public LedgersRequestBuilder ledgers() {
        return server.ledgers();
    }

    @Override
    public OffersRequestBuilder offers() {
        return server.offers();
    }

    @Override
    public OperationsRequestBuilder operations() {
        return server.operations();
    }

    @Override
    public FeeStatsRequestBuilder feeStats() {
        return server.feeStats();
    }

    @Override
    public OrderBookRequestBuilder orderBook() {
        return server.orderBook();
    }

    @Override
    public TradesRequestBuilder trades() {
        return server.trades();
    }

    @Override
    public PaymentsRequestBuilder payments() {
        return server.payments();
    }

    @Override
    public TransactionsRequestBuilder transactions() {
        return server.transactions();
    }

    @Override
    public SubmitTransactionResponse submitTransaction(Transaction transaction) throws IOException, AccountRequiresMemoException {
        return server.submitTransaction(transaction);
    }

    @Override
    public Network getNetwork() {
        return horizonServer.getNetwork();
    }
}
