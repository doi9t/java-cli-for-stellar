/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.adapters;

import ca.watier.tools.exceptions.ArgumentException;
import ca.watier.tools.exceptions.InvalidNumberOfArgumentException;
import ca.watier.tools.handlers.ConsoleIOHandlerImpl;
import ca.watier.tools.models.ApplicationParameters;
import ca.watier.utils.collections.MultiValueCollection;
import ca.watier.utils.collections.MultiValueCollectionImpl;
import org.apache.commons.cli.*;

import java.util.Objects;

public class ApplicationParameterAdapterImpl implements ParameterAdapter {
    private final String[] args;

    public ApplicationParameterAdapterImpl(String[] args) {
        this.args = Objects.requireNonNullElse(args, new String[0]);
    }

    @Override
    public MultiValueCollection<ApplicationParameters, String> adapt() throws ArgumentException {
        MultiValueCollection<ApplicationParameters, String> parameters = MultiValueCollectionImpl.arrayList();

        if (args.length == 0) {
            return parameters;
        }

        for (org.apache.commons.cli.Option option : getCommandLine(args).getOptions()) {
            String longOpt = option.getLongOpt();
            String[] values = option.getValues();

            ApplicationParameters properties = ApplicationParameters.fromKey(longOpt);

            if (properties == null) {
                continue;
            }

            if (values == null) {
                parameters.add(properties);
            } else {
                parameters.add(properties, values);
            }
        }

        return parameters;
    }

    private CommandLine getCommandLine(String[] args) throws ArgumentException {
        try {
            CommandLineParser parser = new DefaultParser();
            return parser.parse(ConsoleIOHandlerImpl.OPTIONS, args);
        } catch (MissingArgumentException e) {
            org.apache.commons.cli.Option option = e.getOption();
            throw new InvalidNumberOfArgumentException(option.getLongOpt(), e);
        } catch (ParseException e) {
            throw new IllegalStateException("Unable to parse the parameters", e);
        }
    }
}
