/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

import ca.watier.tools.adapters.ApplicationParameterAdapterImpl;
import ca.watier.tools.adapters.ParameterAdapter;
import ca.watier.tools.exceptions.ArgumentException;
import ca.watier.tools.handlers.ConsoleIOHandlerImpl;
import ca.watier.tools.handlers.IOHandler;
import ca.watier.tools.handlers.StellarServerHandler;
import ca.watier.tools.handlers.StellarServerHandlerImpl;
import ca.watier.tools.models.ApplicationParameters;
import ca.watier.utils.collections.MultiValueCollection;

public class StellarCli {
    private static final String TEMPLATE_ERROR_INVALID_NUMBER_OF_ARGUMENT = "Missing arguments for the parameter '%s'!%n";

    public static void main(String[] args) {
        IOHandler ioHandler = new ConsoleIOHandlerImpl();
        ParameterAdapter parameterAdapter = new ApplicationParameterAdapterImpl(args);

        try {
            MultiValueCollection<ApplicationParameters, String> parameters = parameterAdapter.adapt();

            StellarServerHandler stellarServerHandler = new StellarServerHandlerImpl(parameters);
            final StellarExecutor stellarExecutor = new StellarExecutor(stellarServerHandler, ioHandler);
            stellarExecutor.execute(parameters);
        } catch (ArgumentException e) {
            ioHandler.errorPrintf(TEMPLATE_ERROR_INVALID_NUMBER_OF_ARGUMENT, e.getParameterName());
        }
    }
}
