/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

import ca.watier.tools.exceptions.*;
import ca.watier.tools.handlers.IOHandler;
import ca.watier.tools.handlers.StellarServerHandler;
import ca.watier.tools.mappers.OperationTypeMapper;
import ca.watier.tools.models.*;
import ca.watier.utils.collections.MultiValueCollection;
import ca.watier.utils.models.KeyValueHolder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;
import org.stellar.sdk.xdr.OperationType;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static ca.watier.tools.models.ApplicationParameters.*;

public class StellarExecutor {

    private final StellarServerHandler stellarServerHandler;
    private final IOHandler ioHandler;

    public StellarExecutor(StellarServerHandler stellarServerHandler, IOHandler ioHandler) {
        this.stellarServerHandler = stellarServerHandler;
        this.ioHandler = ioHandler;
    }

    public void execute(MultiValueCollection<ApplicationParameters, String> parameters) {
        if (parameters.containsKey(HELP)) {
            ioHandler.printHelp();
            return;
        }

        printWarningMessageAndLicense();

        boolean stopIfAnyFailedOption = parameters.containsKey(STOP_ALL_IF_ANY_FAIL);
        Network network = stellarServerHandler.getNetwork();

        List<StellarOperation> stellarActions = buildOperationFrom(parameters, stopIfAnyFailedOption);

        if (haveAtLeastOneOperation(stellarActions)) {
            printSummaryAndExecuteOperationsIfConfirmed(stopIfAnyFailedOption, network, stellarActions);
        }
    }

    protected List<StellarOperation> buildOperationFrom(MultiValueCollection<ApplicationParameters, String> parameters, boolean stopIfAnyFailedOption) {
        List<StellarOperation> stellarActions = new ArrayList<>();
        ca.watier.tools.models.Transaction currentTransaction = null;
        boolean isCurrentTransactionAddedToTheList = false;

        // Bind the operation to a transaction; based on the source parameter.
        for (KeyValueHolder<ApplicationParameters, String> optionEntry : parameters) {
            ApplicationParameters applicationParameters = optionEntry.getKey();
            List<String> arguments = optionEntry.getValues();

            // Since flags are checked on the fly, we skip them.
            if (applicationParameters == null || applicationParameters.isFlag()) {
                continue;
            }

            if (applicationParameters == CREATE_KEYPAIR) {
                KeyPair randomKeyPair = KeyPair.random();
                printKeyPair(randomKeyPair);
                continue;
            }

            // Add only the first time we encounter any operation, in a transaction
            if (!isCurrentTransactionAddedToTheList && currentTransaction != null && applicationParameters.isOperation()) {
                isCurrentTransactionAddedToTheList = true; //TODO: Uses a set + uuid instead ?
                stellarActions.add(currentTransaction);
            }

            try {
                if (applicationParameters == PRINT_BALANCE) {
                    String sourcePublicKey = arguments.get(0);
                    stellarActions.add(new PrintBalance(sourcePublicKey));
                } else if (applicationParameters == SOURCE) {
                    KeyPair source = buildKeyPairFromPrivate(arguments.get(0));
                    currentTransaction = new ca.watier.tools.models.Transaction(source);
                    isCurrentTransactionAddedToTheList = false;
                } else if (currentTransaction == null) {
                    ioHandler.errorPrintf(Constants.TEMPLATE_ERROR_SOURCE_NOT_DEFINED, applicationParameters.name());
                } else if (applicationParameters == MEMO) {
                    currentTransaction.setMemo(arguments.get(0));
                } else if (applicationParameters == PAYMENT || applicationParameters == CREATE_ACCOUNT) {
                    KeyPair source = currentTransaction.getSourceKeypair();
                    Operation operation = sendAmountTo(applicationParameters, source, parameters, arguments.get(0), arguments.get(1));
                    OperationHolder holder = OperationHolder.from(operation, OperationTypeMapper.map(applicationParameters));
                    currentTransaction.addOperation(holder);
                }
            } catch (StellarException e) {
                logByException(e);

                if (stopIfAnyFailedOption) {
                    break;
                }
            }
        }
        return stellarActions;
    }

    protected void printSummaryAndExecuteOperationsIfConfirmed(boolean stopIfAnyFailedOption, Network network, List<StellarOperation> stellarActions) {
        ioHandler.println(Constants.MSG_SUMMARY_OF_THE_TRANSACTION_TITLE);

        printOperationsSummary(stellarActions);

        YesNoResponse userConfirmationForTransactions = ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX);

        if (YesNoResponse.YES == userConfirmationForTransactions) {
            for (StellarOperation stellarOperation : stellarActions) {
                try {
                    if (stellarOperation instanceof ca.watier.tools.models.Transaction) {
                        ca.watier.tools.models.Transaction transaction = (ca.watier.tools.models.Transaction) stellarOperation;
                        KeyPair source = transaction.getSourceKeypair();
                        List<Operation> operations = new ArrayList<>();

                        for (OperationHolder holder : transaction.getOperations()) {
                            operations.add(holder.getOperation());
                        }

                        sendTransaction(network, source, operations);
                    } else if (stellarOperation instanceof PrintBalance) {
                        String sourcePublicKey = ((PrintBalance) stellarOperation).getSourcePublicKey();
                        printBalanceOf(sourcePublicKey);
                    }
                } catch (StellarException e) {
                    logByException(e);

                    if (stopIfAnyFailedOption) {
                        break;
                    }
                }
            }
        } else if (YesNoResponse.NO == userConfirmationForTransactions) {
            ioHandler.errorPrintln(Constants.MSG_EXITING_NO_CONFIRMATION);
        } else {
            ioHandler.errorPrintln(Constants.ERROR_UNABLE_TO_VALIDATE_CONFIRMATION);
        }
    }

    private boolean haveAtLeastOneOperation(List<StellarOperation> transactions) {
        return transactions != null && !transactions.isEmpty();
    }

    private void printOperationsSummary(List<StellarOperation> transactions) {
        for (StellarOperation stellarOperation : transactions) {
            if (stellarOperation instanceof ca.watier.tools.models.Transaction) {

                ca.watier.tools.models.Transaction transaction = (ca.watier.tools.models.Transaction) stellarOperation;

                for (OperationHolder holder : transaction.getOperations()) {
                    Operation operation = holder.getOperation();
                    OperationType operationType = holder.getOperationType();

                    switch (operationType) {
                        case CREATE_ACCOUNT -> {
                            CreateAccountOperation createAccountOperation = (CreateAccountOperation) operation;
                            String startingBalance = createAccountOperation.getStartingBalance();
                            String destination = createAccountOperation.getDestination();
                            ioHandler.printf(Constants.TEMPLATE_ACCOUNT_CREATION_SUMMARY, startingBalance, destination);
                        }
                        case PAYMENT -> {
                            PaymentOperation paymentOperation = (PaymentOperation) operation;
                            String amount = paymentOperation.getAmount();
                            String destination = paymentOperation.getDestination();
                            ioHandler.printf(Constants.TEMPLATE_PAYMENT_SUMMARY, amount, destination);
                        }
                        default -> ioHandler.errorPrintln(Constants.ERROR_TEMPLATE_UNKNOWN_OPERATION_SUMMARY);
                    }
                }
            } else if (stellarOperation instanceof PrintBalance) {
                ioHandler.printf(Constants.TEMPLATE_PRINT_BALANCE_SUMMARY, ((PrintBalance) stellarOperation).getSourcePublicKey());
            }
        }
    }

    private void sendTransaction(Network network, KeyPair source, Collection<Operation> operations) throws TransactionException, AccountException {
        if (CollectionUtils.isEmpty(operations)) {
            return;
        }

        validateOperations(operations);
        sendTransaction(network, source, operations.toArray(new Operation[]{}));
    }

    private void validateOperations(Collection<Operation> operations) throws TransactionException {
        if (CollectionUtils.size(operations) > 20) { //TODO: When > max, ask to do another transaction ?
            throw new TransactionHaveToManyOperationsException();
        }
    }

    private void printKeyPair(KeyPair pair) {
        ioHandler.printf(Constants.TEMPLATE_MESSAGE_PUBLIC_KEY, pair.getAccountId());
        ioHandler.printf(Constants.TEMPLATE_MESSAGE_PRIVATE_KEY, new String(pair.getSecretSeed()));
    }

    private void printBalanceOf(String publicKey) throws StellarException {
        KeyPair pair = buildKeyPairFromPublic(publicKey);

        AccountResponse account = fetchAccount(pair.getAccountId());
        ioHandler.printf(Constants.TEMPLATE_BALANCES_ACCOUNT, pair.getAccountId());

        AccountResponse.Balance[] balances = account.getBalances();

        if (ArrayUtils.isNotEmpty(balances)) {
            for (AccountResponse.Balance balance : balances) {
                ioHandler.printf(
                        Constants.TEMPLATE_BALANCES_TYPE_AMOUNT,
                        balance.getAssetType(),
                        balance.getBalance()
                );
            }
        } else {
            ioHandler.errorPrintln(Constants.ERROR_THERES_NO_BALANCE);
        }
    }

    private Operation sendAmountTo(ApplicationParameters option,
                                   KeyPair sourceKeyPair,
                                   MultiValueCollection<ApplicationParameters, String> applicationParameters,
                                   String destinationPublicKey,
                                   String amount) throws StellarException {
        validateAmount(amount);

        String sourcePublicKey = sourceKeyPair.getAccountId();

        if (PAYMENT == option) {
            try {
                validateAccount(destinationPublicKey);
            } catch (AccountNotFoundException ex) {
                if (applicationParameters.containsKey(CREATE_ACCOUNT_IF_NOT_FOUND)) {
                    return createAccountOperation(destinationPublicKey, amount, sourcePublicKey);
                } else {
                    throw ex;
                }
            }

            return paymentOperation(sourcePublicKey, destinationPublicKey, amount);
        } else if (CREATE_ACCOUNT == option) {
            return createAccountOperation(destinationPublicKey, amount, sourcePublicKey);
        } else {
            throw new TransactionWithNoOperationException();
        }
    }

    private CreateAccountOperation createAccountOperation(String destinationPublicKey, String amount, String sourcePublicKey) throws StellarException {
        validatePublicKey(destinationPublicKey);
        return accountCreationOperation(sourcePublicKey, destinationPublicKey, amount);
    }

    private void validateAmount(String amount) throws InvalidAmountException {
        // https://stackoverflow.com/a/30471139
        if (!NumberUtils.isCreatable(amount) || Math.max(0, new BigDecimal(amount).stripTrailingZeros().scale()) > 7) {
            throw new InvalidAmountException(amount);
        }
    }

    private void logByException(StellarException e) {
        if (e instanceof AccountNotFoundException) {
            ioHandler.errorPrintf(Constants.TEMPLATE_ERROR_UNABLE_TO_FIND_ACCOUNT, ((AccountNotFoundException) e).getPublicKey());
        } else if (e instanceof InvalidPrivateKeyFormatException) {
            ioHandler.errorPrintf(Constants.TEMPLATE_ERROR_INVALID_PRIVATE_KEY, ((InvalidPrivateKeyFormatException) e).getKey());
        } else if (e instanceof InvalidPublicKeyFormatException) {
            ioHandler.errorPrintf(Constants.TEMPLATE_ERROR_INVALID_PUBLIC_KEY, ((InvalidPublicKeyFormatException) e).getKey());
        } else if (e instanceof KeyException) {
            ioHandler.errorPrintf(Constants.TEMPLATE_UNKNOWN_KEY_ERROR, ((KeyException) e).getKey());
        } else if (e instanceof InvalidAmountException) {
            ioHandler.errorPrintf(Constants.ERROR_SPECIFIED_AMOUNT, ((InvalidAmountException) e).getAmount());
        } else if (e instanceof TransactionWithNoOperationException) {
            ioHandler.errorPrintln(Constants.ERROR_TRANSACTION_DOESNT_HAVE_AT_LEAST_ONE_OPERATION);
        } else if (e instanceof TransactionNotSentException) {
            ioHandler.errorPrintln(Constants.ERROR_UNABLE_TO_SEND_THE_TRANSACTION);
        } else if (e instanceof TransactionRequireMemoException) {
            ioHandler.errorPrintln(Constants.ERROR_TRANSACTION_REQUIRE_A_MEMO);
        } else if (e instanceof TransactionHaveToManyOperationsException) {
            ioHandler.errorPrintln(Constants.ERROR_TRANSACTION_HAS_MORE_OPERATIONS_THAN_MAX);
        } else {
            ioHandler.errorPrintln(Constants.ERROR_UNKNOWN);
        }
    }


    private KeyPair buildKeyPairFromPublic(String publicKey) throws KeyException {
        try {
            return KeyPair.fromAccountId(publicKey);
        } catch (java.lang.RuntimeException e) {
            throw new InvalidPublicKeyFormatException(publicKey);
        } catch (Exception e) {
            throw new KeyException(publicKey);
        }
    }

    /**
     * Method to fetch the account on the specified server
     *
     * @param publicKey The account public key
     * @throws AccountNotFoundException if the account is not present / not able to fetch the account for any reasons
     */
    private AccountResponse fetchAccount(String publicKey) throws AccountException {
        try {
            return stellarServerHandler.accounts().account(publicKey);
        } catch (IOException | org.stellar.sdk.requests.ErrorResponse e) {
            throw new AccountNotFoundException(publicKey);
        }
    }

    /**
     * Validate the specified account; first we validate the public key, second we validate if the account is present.
     *
     * @param publicKey The public key, to be validated
     * @throws AccountException
     */
    private void validateAccount(String publicKey) throws AccountException, KeyException {
        validatePublicKey(publicKey);
        fetchAccount(publicKey);
    }

    private PaymentOperation paymentOperation(String sourcePublicKey, String destinationPublicKey, String amount) throws StellarException {
        try {
            return new PaymentOperation.Builder(destinationPublicKey, new AssetTypeNative(), amount)
                    .setSourceAccount(sourcePublicKey)
                    .build();
        } catch (Exception e) {
            throw new StellarException();
        }
    }

    private KeyPair buildKeyPairFromPrivate(String privateKey) throws KeyException {
        try {
            return KeyPair.fromSecretSeed(privateKey);
        } catch (java.lang.RuntimeException e) {
            throw new InvalidPrivateKeyFormatException(privateKey);
        } catch (Exception e) {
            throw new KeyException(privateKey);
        }
    }

    private void sendTransaction(Network network, KeyPair source, Operation... operations) throws TransactionException, AccountException {

        if (ArrayUtils.isEmpty(operations)) {
            throw new TransactionWithNoOperationException();
        }

        try {
            TransactionBuilder transactionBuilder = transactionFrom(network, fetchAccount(source.getAccountId()));

            for (Operation operation : operations) {
                transactionBuilder.addOperation(operation);
            }

            Transaction transaction = transactionBuilder.build();
            transaction.sign(source);

            SubmitTransactionResponse submitTransactionResponse = stellarServerHandler.submitTransaction(transaction);

            Optional<SubmitTransactionResponse.Extras> optionalExtras =
                    Optional.ofNullable(submitTransactionResponse)
                            .map(SubmitTransactionResponse::getExtras);

            // https://developers.stellar.org/api/errors/result-codes/
            if (optionalExtras.isPresent()) { // The transaction is failed
                SubmitTransactionResponse.Extras extras = optionalExtras.get();
                SubmitTransactionResponse.Extras.ResultCodes resultCodes = extras.getResultCodes();
                List<String> operationsResultCodes = resultCodes.getOperationsResultCodes();
                String transactionResultCode = resultCodes.getTransactionResultCode();

                TransactionResultCodes resultCode = TransactionResultCodes.fromCode(transactionResultCode);

                if (TransactionResultCodes.FAILED == resultCode) {
                    ioHandler.errorPrintln(Constants.ERROR_TRX_FAILED);
                    for (String operationsResultCode : operationsResultCodes) {

                        if ("op_success".equalsIgnoreCase(operationsResultCode)) {
                            continue; // Show only the failed operation (The transaction is not executed anyway)
                        }

                        ioHandler.errorPrintf(Constants.ERROR_TEMPLATE_TRC_FAILED, operationsResultCode);
                    }
                }
            }

        } catch (IOException |
                 org.stellar.sdk.responses.SubmitTransactionTimeoutResponseException |
                 org.stellar.sdk.responses.SubmitTransactionUnknownResponseException e) {
            throw new TransactionNotSentException();
        } catch (AccountRequiresMemoException e) {
            throw new TransactionRequireMemoException();
        }
    }

    private CreateAccountOperation accountCreationOperation(String sourcePublicKey, String destinationPublicKey, String amount) throws StellarException {
        try {
            return new CreateAccountOperation.Builder(destinationPublicKey, amount).
                    setSourceAccount(sourcePublicKey)
                    .build();
        } catch (Exception e) {
            throw new StellarException();
        }
    }

    /**
     * Wrapper method, that is only intended to make the code more readable; builds the key and throws the exception if the build fail.
     *
     * @param publicKey The public key, to be validated
     * @see StellarExecutor#buildKeyPairFromPublic(java.lang.String)
     */
    private void validatePublicKey(String publicKey) throws KeyException {
        buildKeyPairFromPublic(publicKey);
    }

    private TransactionBuilder transactionFrom(Network network, AccountResponse sourceAccount) {
        //TODO: Add a better fee handling ? -> https://developers.stellar.org/docs/glossary/fees/
        TimeBounds timeBounds = TimeBounds.expiresAfter(180);
        TransactionPreconditions transactionPreconditions = TransactionPreconditions.builder().timeBounds(timeBounds).build();
        return new TransactionBuilder(sourceAccount, network).addPreconditions(transactionPreconditions).setBaseFee(AbstractTransaction.MIN_BASE_FEE);
    }

    private void printWarningMessageAndLicense() {
        ioHandler.errorPrintln(Constants.MSG_WARNING_USES_THIS_CLI_AT_YOUR_OWN_RISK);
        ioHandler.errorPrintln(Constants.MSG_THIS_SOFTWARE_IS_UNDER_THE_APACHE_LICENSE);
        ioHandler.println(Constants.MSG_PRESS_THE_ENTER_KEY_TO_CONTINUE);
        pressEnterToContinue();
    }

    private void pressEnterToContinue() {
        try {
            ioHandler.readNextByte();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
