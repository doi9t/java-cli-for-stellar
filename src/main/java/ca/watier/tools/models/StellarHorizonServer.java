/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.models;

import org.stellar.sdk.Network;

public enum StellarHorizonServer {
    TEST("https://horizon-testnet.stellar.org", Network.TESTNET),
    PUBLIC("https://horizon.stellar.org", Network.PUBLIC);

    private final String url;
    private final Network network;

    StellarHorizonServer(String url, Network network) {
        this.url = url;
        this.network = network;
    }

    public Network getNetwork() {
        return network;
    }

    public String getUrl() {
        return url;
    }
}
