/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.models;

public enum YesNoResponse {
    YES("yes", "y"),
    NO("no", "n");

    private final String longName;
    private final String shortName;

    YesNoResponse(String longName, String shortName) {
        this.longName = longName;
        this.shortName = shortName;
    }

    public static YesNoResponse from(String key) {
        if (key == null || key.isEmpty()) {
            return null;
        }

        for (YesNoResponse value : YesNoResponse.values()) {
            if (value.longName.equalsIgnoreCase(key) || value.shortName.equalsIgnoreCase(key)) {
                return value;
            }
        }

        return null;
    }
}
