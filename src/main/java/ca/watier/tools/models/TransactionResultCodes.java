/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.models;

/**
 * Documentation: https://developers.stellar.org/api/errors/result-codes/transactions/
 */
public enum TransactionResultCodes {
    SUCCESS("tx_success", "The transaction succeeded"),
    FAILED("tx_failed", "One of the operations failed (none were applied)"),
    TOO_EARLY("tx_too_early", "The ledger closeTime was before the minTime"),
    TOO_LATE("tx_too_late", "The ledger closeTime was after the maxTime"),
    MISSING_OPERATION("tx_missing_operation", "No operation was specified"),
    BAD_SEQ("tx_bad_seq", "sequence number does not match source account"),
    BAD_AUTH("tx_bad_auth", "too few valid signatures / wrong network"),
    INSUFFICIENT_BALANCE("tx_insufficient_balance", "fee would bring account below reserve"),
    NO_ACCOUNT("tx_no_source_account", "source account not found"),
    INSUFFICIENT_FEE("tx_insufficient_fee", "fee is too small"),
    BAD_AUTH_EXTRA("tx_bad_auth_extra", "unused signatures attached to transaction"),
    INTERNAL_ERROR("tx_internal_error", "an unknown error occured");

    private final String code;

    TransactionResultCodes(String code, String description) {
        this.code = code;
    }

    public static TransactionResultCodes fromCode(String code) {
        if (code == null || code.isEmpty()) {
            return null;
        }

        for (TransactionResultCodes value : TransactionResultCodes.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }

        return null;
    }

    public String getCode() {
        return code;
    }
}
