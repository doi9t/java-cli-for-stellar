/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.models;

import java.util.Objects;

/**
 * This class define the accepted application parameters
 */
public enum ApplicationParameters {
    TEST(null,
            "on-horizon-test-server",
            "Uses the Stellar Horizon test server; this will be applied on ALL transactions / operations",
            0,
            null,
            ParameterType.FLAG,
            false
    ),
    STOP_ALL_IF_ANY_FAIL(null,
            "stop-all-if-any-fail",
            "Stop all tasks if any fail",
            0,
            null,
            ParameterType.FLAG,
            false),
    CREATE_ACCOUNT_IF_NOT_FOUND(null,
            "create-account-if-destination-not-found",
            """
                    Create a new account, if the destination account is not present when sending XLM; the source
                    account will be the founder. This flag will be applied on ALL transactions / operations
                    """,
            0,
            null,
            ParameterType.FLAG,
            false),
    HELP(null,
            "help",
            "Print this help",
            0,
            null,
            ParameterType.FLAG,
            false),
    CREATE_KEYPAIR(null,
            "create-keypair",
            "Create a new key pair, and print to the console",
            0,
            null,
            ParameterType.STELLAR_LOCAL_ACTION,
            false),
    PRINT_BALANCE(
            null,
            "print-balance",
            "Print the account balances",
            1,
            "SOURCE public key",
            ParameterType.STELLAR_API_ACTION,
            false),
    SOURCE(
            null,
            "source",
            "The private key of the source account.",
            1,
            "SOURCE private key",
            ParameterType.STELLAR_OPERATION,
            false),
    MEMO(
            null,
            "memo",
            "The memo for the current transaction.",
            1,
            "MEMO",
            ParameterType.STELLAR_OPERATION,
            false),
    CREATE_ACCOUNT(
            null,
            "create-account",
            "Create a new account with a specific amount.",
            2,
            "DESTINATION public key> <amount in XLM",
            ParameterType.STELLAR_OPERATION,
            false),
    PAYMENT(
            null,
            "payment",
            String.format("""
                    Send an amount to the specified destination; if the destination account is not funded,
                    the transaction will not be sent (no cost). You can create a new account in this case
                    using the "%s" flag
                    """, CREATE_ACCOUNT_IF_NOT_FOUND.longOption),
            2,
            "DESTINATION public key> <amount in XLM",
            ParameterType.STELLAR_OPERATION,
            false);

    /**
     * For the moment, we don’t use the short options, can be handy in the future.
     */
    private final String shortOption;
    private final String longOption;
    private final String description;
    private final int nbOfArgs;
    private final String argName;
    private final boolean isArgumentsOptional;
    private final ParameterType type;

    ApplicationParameters(String shortOption, String longOption, String description, int nbOfArgs, String argName, ParameterType type, boolean isArgumentsOptional) {
        this.shortOption = shortOption;
        this.longOption = longOption;
        this.description = description;
        this.nbOfArgs = nbOfArgs;
        this.argName = argName;
        this.isArgumentsOptional = isArgumentsOptional;
        this.type = type;
    }

    public static ApplicationParameters fromKey(String key) {
        for (ApplicationParameters value : ApplicationParameters.values()) {
            if (Objects.equals(value.getShortOption(), key) || Objects.equals(value.getLongOption(), key)) {
                return value;
            }
        }
        return null;
    }

    public String getShortOption() {
        return shortOption;
    }

    public String getLongOption() {
        return longOption;
    }

    @Override
    public String toString() {
        return "OptionWithProperties{" +
                "shortOption='" + shortOption + '\'' +
                ", longOption='" + longOption + '\'' +
                '}';
    }

    public String getArgName() {
        return argName;
    }

    public String getDescription() {
        return description;
    }

    public int getNbOfArgs() {
        return nbOfArgs;
    }

    public boolean isArgumentsOptional() {
        return isArgumentsOptional;
    }

    public boolean isFlag() {
        return ParameterType.FLAG == type;
    }

    public boolean isOperation() {
        return ParameterType.STELLAR_OPERATION == type;
    }

    public ParameterType getType() {
        return type;
    }
}
