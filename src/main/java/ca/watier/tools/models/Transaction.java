/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.models;

import org.stellar.sdk.KeyPair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transaction implements StellarOperation {
    private final KeyPair sourceKeypair;
    private final List<OperationHolder> operations = new ArrayList<>();
    private String memo;

    public Transaction(KeyPair sourceKeypair) {
        this.sourceKeypair = sourceKeypair;
    }

    public void addOperation(OperationHolder operationHolder) {
        if (operationHolder == null) {
            return;
        }

        operations.add(operationHolder);
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<OperationHolder> getOperations() {
        return Collections.unmodifiableList(operations);
    }

    public KeyPair getSourceKeypair() {
        return sourceKeypair;
    }
}
