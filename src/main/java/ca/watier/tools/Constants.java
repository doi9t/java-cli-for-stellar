/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

public class Constants {
    public static final String CONFIRMATION_EXECUTION_TRX = "Do you want to execute, the operation(s) listed in the summary, on the server ? [`yes`, `y`, `no` or `n`]";
    public static final String ERROR_TRX_FAILED = "The transaction failed with the reason(s)";
    public static final String TEMPLATE_MESSAGE_PUBLIC_KEY = "Public key: %s%n";
    public static final String TEMPLATE_MESSAGE_PRIVATE_KEY = "Private key: %s%n";
    public static final String TEMPLATE_ERROR_UNABLE_TO_FIND_ACCOUNT = "Unable to find the specified account [%s]!%n";
    public static final String TEMPLATE_ERROR_INVALID_PRIVATE_KEY = "The specified private key [%s] is invalid!%n";
    public static final String TEMPLATE_ERROR_INVALID_PUBLIC_KEY = "The specified public key [%s] is invalid!%n";
    public static final String TEMPLATE_UNKNOWN_KEY_ERROR = "Unknown key error [%s]!%n";
    public static final String ERROR_SPECIFIED_AMOUNT = "The specified amount [%s] is invalid!%n";
    public static final String ERROR_TRANSACTION_DOESNT_HAVE_AT_LEAST_ONE_OPERATION = "The transaction doesn't have at least one operation!";
    public static final String ERROR_UNABLE_TO_SEND_THE_TRANSACTION = "Unable to send the transaction!";
    public static final String ERROR_TRANSACTION_REQUIRE_A_MEMO = "The transaction require a memo!";
    public static final String ERROR_TRANSACTION_HAS_MORE_OPERATIONS_THAN_MAX = "The transaction has more operations than the max (20 operations MAX)!";
    public static final String ERROR_UNKNOWN = "Unknown error!";
    public static final String MSG_SUMMARY_OF_THE_TRANSACTION_TITLE = "=== Summary of the transaction(s) ===";
    public static final String MSG_EXITING_NO_CONFIRMATION = "Exiting (The transactions were not executed)";
    public static final String ERROR_UNABLE_TO_VALIDATE_CONFIRMATION = "Unable to validate your answer, exiting (The transactions were not executed)";
    public static final String TEMPLATE_ACCOUNT_CREATION_SUMMARY = " - Account creation %s XLM to %s: %n";
    public static final String TEMPLATE_PAYMENT_SUMMARY = " - Payment of %s XLM to %s%n";
    public static final String TEMPLATE_PRINT_BALANCE_SUMMARY = " - Print the balance of %s%n";
    public static final String ERROR_TEMPLATE_UNKNOWN_OPERATION_SUMMARY = " - Unable to print the operation details!%n";
    public static final String TEMPLATE_BALANCES_ACCOUNT = "Balances for account: %s%n";
    public static final String TEMPLATE_BALANCES_TYPE_AMOUNT = "\tType: %s,%n\tBalance: %s XLM%n";
    public static final String ERROR_THERES_NO_BALANCE = "There's no balance!";
    public static final String ERROR_TEMPLATE_TRC_FAILED = "  - %s%n";
    public static final String MSG_WARNING_USES_THIS_CLI_AT_YOUR_OWN_RISK = "WARNING: Uses this CLI AT YOUR OWN RISK!";
    public static final String MSG_THIS_SOFTWARE_IS_UNDER_THE_APACHE_LICENSE = "This software is under the Apache License 2.0; available at: https://www.apache.org/licenses/LICENSE-2.0";
    public static final String MSG_PRESS_THE_ENTER_KEY_TO_CONTINUE = "Press the [ENTER] key to continue";
    public static final String TEMPLATE_ERROR_SOURCE_NOT_DEFINED = "The operation '%s' is skipped, please define a source before!";
}
