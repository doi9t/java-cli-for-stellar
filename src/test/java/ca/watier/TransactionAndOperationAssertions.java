/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier;

import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.Operation;
import org.stellar.sdk.PaymentOperation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class TransactionAndOperationAssertions {
    public static void assertAccountCreationOperation(Operation operation, String sourceAccount, String destinationAccount, String startingBalance) {
        if (!(operation instanceof CreateAccountOperation)) {
            fail("The operation is not an account creation!");
        }

        CreateAccountOperation createAccountOperation = (CreateAccountOperation) operation;
        assertThat(createAccountOperation.getSourceAccount()).isEqualTo(sourceAccount);
        assertThat(createAccountOperation.getDestination()).isEqualTo(destinationAccount);
        assertThat(createAccountOperation.getStartingBalance()).isEqualTo(startingBalance);
    }

    public static void assertPaymentOperation(Operation operation, String sourceAccount, String destinationAccount, String amount) {
        if (!(operation instanceof PaymentOperation)) {
            fail("The operation is not an account creation!");
        }

        PaymentOperation paymentOperation = (PaymentOperation) operation;
        assertThat(paymentOperation.getSourceAccount()).isEqualTo(sourceAccount);
        assertThat(paymentOperation.getDestination()).isEqualTo(destinationAccount);
        assertThat(paymentOperation.getAmount()).isEqualTo(amount);
    }
}
