/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier;

import ca.watier.tools.models.ApplicationParameters;
import ca.watier.utils.collections.MultiValueCollection;

public class TransactionAndOperationUtils {

    public static void addSourceParameter(MultiValueCollection<ApplicationParameters, String> multiValueMap, String sourcePrivateKey) {
        if (multiValueMap == null) {
            return;
        }

        multiValueMap.add(ApplicationParameters.SOURCE, sourcePrivateKey);
    }


    public static void addCreateAccountParameter(MultiValueCollection<ApplicationParameters, String> multiValueMap, String destinationPublicKey, String amount) {
        if (multiValueMap == null) {
            return;
        }

        multiValueMap.add(ApplicationParameters.CREATE_ACCOUNT, destinationPublicKey, amount);
    }

    public static void addSendParameter(MultiValueCollection<ApplicationParameters, String> multiValueMap, String destinationPublicKey, String amount) {
        if (multiValueMap == null) {
            return;
        }

        multiValueMap.add(ApplicationParameters.PAYMENT, destinationPublicKey, amount);
    }

    public static void addPrint(MultiValueCollection<ApplicationParameters, String> multiValueMap, String publicKey) {
        if (multiValueMap == null) {
            return;
        }

        multiValueMap.add(ApplicationParameters.PRINT_BALANCE, publicKey);
    }
}
