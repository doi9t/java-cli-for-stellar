/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

import ca.watier.TransactionAndOperationUtils;
import ca.watier.tools.handlers.IOHandler;
import ca.watier.tools.handlers.StellarServerHandler;
import ca.watier.tools.models.ApplicationParameters;
import ca.watier.tools.models.YesNoResponse;
import ca.watier.utils.collections.MultiValueCollection;
import ca.watier.utils.collections.MultiValueCollectionImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stellar.sdk.*;
import org.stellar.sdk.requests.AccountsRequestBuilder;
import org.stellar.sdk.responses.AccountResponse;

import java.io.IOException;
import java.util.List;

import static ca.watier.TransactionAndOperationAssertions.assertAccountCreationOperation;
import static ca.watier.TransactionAndOperationAssertions.assertPaymentOperation;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.*;

/**
 * Tests to check if the {@link Operation} is correctly created with multiples transactions; the entire flow is tested (except the sending)
 */
@ExtendWith(MockitoExtension.class)
class StellarExecutorCompeteMultipleTransactionsTest {

    @Captor
    private ArgumentCaptor<Transaction> sendTransactionCaptor;

    @Mock
    private AccountsRequestBuilder accountsRequestBuilder;

    @Mock
    private StellarServerHandler stellarServerHandler;

    @Mock
    private IOHandler ioHandler;

    private StellarExecutor stellarExecutor;

    private final Network network = new Network("unit-test-network");

    @BeforeEach
    void setUp() {
        stellarExecutor = spy(new StellarExecutor(stellarServerHandler, ioHandler));
    }

    @Test
    void multiple_trx() throws IOException, AccountRequiresMemoException {
        // given
        AccountResponse givenFirstSourceAccountResponse = mock(AccountResponse.class);
        AccountResponse givenSecondSourceAccountResponse = mock(AccountResponse.class);
        AccountResponse givenThirdSourceAccountResponse = mock(AccountResponse.class);

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());

        KeyPair givenSecondSourceKeyPair = KeyPair.random();
        String givenSecondSourcePublicKey = givenSecondSourceKeyPair.getAccountId();
        String givenSecondSourcePrivateKey = new String(givenSecondSourceKeyPair.getSecretSeed());

        KeyPair givenThirdSourceKeyPair = KeyPair.random();
        String givenThirdSourcePublicKey = givenThirdSourceKeyPair.getAccountId();
        String givenThirdSourcePrivateKey = new String(givenThirdSourceKeyPair.getSecretSeed());


        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        /*
            # Trx 1
                ## Account creation 2 (41.1234142 XLM)
                ## Account creation 3 (120.0000000 XLM)
                ## Send to 2 (2.0000000 XLM)
          */
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addCreateAccountParameter(givenParams, givenSecondSourcePublicKey, "41.1234142");
        TransactionAndOperationUtils.addCreateAccountParameter(givenParams, givenThirdSourcePublicKey, "120.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenSecondSourcePublicKey, "2.0000000");

        /*
            # Trx 2 (Account 2)
                ## Send to 1
                ## Send to 3

        */
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenSecondSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, "3.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenThirdSourcePublicKey, "4.0000000");

        /*
            # Trx 3 (Account 3)
                ## Send to 2
                ## Send to 1
         */
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenThirdSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenSecondSourcePublicKey, "5.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, "6.0000000");


        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);

        when(givenFirstSourceAccountResponse.getAccountId()).thenReturn(givenFirstSourcePublicKey);
        when(givenSecondSourceAccountResponse.getAccountId()).thenReturn(givenSecondSourcePublicKey);
        when(givenThirdSourceAccountResponse.getAccountId()).thenReturn(givenThirdSourcePublicKey);

        when(accountsRequestBuilder.account(givenFirstSourcePublicKey)).thenReturn(givenFirstSourceAccountResponse);
        when(accountsRequestBuilder.account(givenSecondSourcePublicKey)).thenReturn(givenSecondSourceAccountResponse);
        when(accountsRequestBuilder.account(givenThirdSourcePublicKey)).thenReturn(givenThirdSourceAccountResponse);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarServerHandler, times(3)).submitTransaction(sendTransactionCaptor.capture());
        List<Transaction> trxToAssert = sendTransactionCaptor.getAllValues();

        // then
        assertWarningMessageIsPrinted();

        assertThat(trxToAssert).hasSize(3);

        // the order of the transactions is not guaranteed
        for (Transaction transaction : trxToAssert) {
            String sourceAccount = transaction.getSourceAccount();
            Operation[] operations = transaction.getOperations();

            if (givenFirstSourcePublicKey.equals(sourceAccount)) {
                assertThat(operations).hasSize(3);
                assertAccountCreationOperation(operations[0], givenFirstSourcePublicKey, givenSecondSourcePublicKey, "41.1234142");
                assertAccountCreationOperation(operations[1], givenFirstSourcePublicKey, givenThirdSourcePublicKey, "120.0000000");
                assertPaymentOperation(operations[2], givenFirstSourcePublicKey, givenSecondSourcePublicKey, "2.0000000");
            } else if (givenSecondSourcePublicKey.equals(sourceAccount)) {
                assertThat(operations).hasSize(2);
                assertPaymentOperation(operations[0], givenSecondSourcePublicKey, givenFirstSourcePublicKey, "3.0000000");
                assertPaymentOperation(operations[1], givenSecondSourcePublicKey, givenThirdSourcePublicKey, "4.0000000");
            } else if (givenThirdSourcePublicKey.equals(sourceAccount)) {
                assertThat(operations).hasSize(2);
                assertPaymentOperation(operations[0], givenThirdSourcePublicKey, givenSecondSourcePublicKey, "5.0000000");
                assertPaymentOperation(operations[1], givenThirdSourcePublicKey, givenFirstSourcePublicKey, "6.0000000");
            } else {
                fail("Unable to find the public key in the operation !");
            }
        }
    }

    @Test
    void first_transaction_missing_source() throws IOException, AccountRequiresMemoException {
        // given
        AccountResponse givenFirstSourceAccountResponse = mock(AccountResponse.class);
        AccountResponse givenSecondSourceAccountResponse = mock(AccountResponse.class);
        AccountResponse givenThirdSourceAccountResponse = mock(AccountResponse.class);

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();

        KeyPair givenSecondSourceKeyPair = KeyPair.random();
        String givenSecondSourcePublicKey = givenSecondSourceKeyPair.getAccountId();
        String givenSecondSourcePrivateKey = new String(givenSecondSourceKeyPair.getSecretSeed());

        KeyPair givenThirdSourceKeyPair = KeyPair.random();
        String givenThirdSourcePublicKey = givenThirdSourceKeyPair.getAccountId();
        String givenThirdSourcePrivateKey = new String(givenThirdSourceKeyPair.getSecretSeed());


        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        /*
            # Trx 1 (NO SOURCE DEFINED)
                ## Account creation 2 (41.1234142 XLM)
                ## Account creation 3 (120.0000000 XLM)
                ## Send to 2 (2.0000000 XLM)
          */
        TransactionAndOperationUtils.addCreateAccountParameter(givenParams, givenSecondSourcePublicKey, "41.1234142");
        TransactionAndOperationUtils.addCreateAccountParameter(givenParams, givenThirdSourcePublicKey, "120.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenSecondSourcePublicKey, "2.0000000");

        /*
            # Trx 2 (Account 2)
                ## Send to 1
                ## Send to 3

        */
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenSecondSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, "3.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenThirdSourcePublicKey, "4.0000000");

        /*
            # Trx 3 (Account 3)
                ## Send to 2
                ## Send to 1
         */
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenThirdSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenSecondSourcePublicKey, "5.0000000");
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, "6.0000000");


        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);

        when(givenSecondSourceAccountResponse.getAccountId()).thenReturn(givenSecondSourcePublicKey);
        when(givenThirdSourceAccountResponse.getAccountId()).thenReturn(givenThirdSourcePublicKey);

        when(accountsRequestBuilder.account(givenFirstSourcePublicKey)).thenReturn(givenFirstSourceAccountResponse);
        when(accountsRequestBuilder.account(givenSecondSourcePublicKey)).thenReturn(givenSecondSourceAccountResponse);
        when(accountsRequestBuilder.account(givenThirdSourcePublicKey)).thenReturn(givenThirdSourceAccountResponse);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarServerHandler, times(2)).submitTransaction(sendTransactionCaptor.capture());
        List<Transaction> trxToAssert = sendTransactionCaptor.getAllValues();

        // then
        assertWarningMessageIsPrinted();

        verify(ioHandler, times(2)).errorPrintf(Constants.TEMPLATE_ERROR_SOURCE_NOT_DEFINED, ApplicationParameters.CREATE_ACCOUNT.name());
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_SOURCE_NOT_DEFINED, ApplicationParameters.PAYMENT.name());

        assertThat(trxToAssert).hasSize(2);

        // the order of the transactions is not guaranteed
        for (Transaction transaction : trxToAssert) {
            String sourceAccount = transaction.getSourceAccount();
            Operation[] operations = transaction.getOperations();

            if (givenSecondSourcePublicKey.equals(sourceAccount)) {
                assertThat(operations).hasSize(2);
                assertPaymentOperation(operations[0], givenSecondSourcePublicKey, givenFirstSourcePublicKey, "3.0000000");
                assertPaymentOperation(operations[1], givenSecondSourcePublicKey, givenThirdSourcePublicKey, "4.0000000");
            } else if (givenThirdSourcePublicKey.equals(sourceAccount)) {
                assertThat(operations).hasSize(2);
                assertPaymentOperation(operations[0], givenThirdSourcePublicKey, givenSecondSourcePublicKey, "5.0000000");
                assertPaymentOperation(operations[1], givenThirdSourcePublicKey, givenFirstSourcePublicKey, "6.0000000");
            } else {
                fail("Unable to find the public key in the operation !");
            }
        }
    }

    private void assertWarningMessageIsPrinted() {
        verify(ioHandler).errorPrintln(Constants.MSG_WARNING_USES_THIS_CLI_AT_YOUR_OWN_RISK);
        verify(ioHandler).errorPrintln(Constants.MSG_THIS_SOFTWARE_IS_UNDER_THE_APACHE_LICENSE);
        verify(ioHandler).println(Constants.MSG_PRESS_THE_ENTER_KEY_TO_CONTINUE);
    }
}