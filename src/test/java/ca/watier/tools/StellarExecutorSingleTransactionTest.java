/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

import ca.watier.tools.handlers.IOHandler;
import ca.watier.tools.handlers.StellarServerHandler;
import ca.watier.tools.models.ApplicationParameters;
import ca.watier.tools.models.YesNoResponse;
import ca.watier.utils.collections.MultiValueCollection;
import ca.watier.utils.collections.MultiValueCollectionImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stellar.sdk.*;
import org.stellar.sdk.requests.AccountsRequestBuilder;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static ca.watier.TransactionAndOperationAssertions.assertAccountCreationOperation;
import static ca.watier.TransactionAndOperationAssertions.assertPaymentOperation;
import static ca.watier.TransactionAndOperationUtils.*;
import static ca.watier.tools.models.TransactionResultCodes.FAILED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


/**
 * Tests to check if the {@link Operation} is correctly created with a single transaction; the entire flow is tested (except the sending)
 */
@ExtendWith(MockitoExtension.class)
class StellarExecutorSingleTransactionTest {

    @Captor
    private ArgumentCaptor<Transaction> sendTransactionCaptor;

    @Mock
    private SubmitTransactionResponse submitTransactionResponse;

    @Mock
    private SubmitTransactionResponse.Extras extras;

    @Mock
    private SubmitTransactionResponse.Extras.ResultCodes resultCodes;

    @Mock
    private AccountsRequestBuilder accountsRequestBuilder;

    @Mock
    private AccountResponse.Balance accountBalance;

    @Mock
    private AccountResponse sourceAccountResponse;

    @Mock
    private AccountResponse destinationAccountResponse;

    @Mock
    private StellarServerHandler stellarServerHandler;

    @Mock
    private IOHandler ioHandler;

    private StellarExecutor stellarExecutor;

    private final KeyPair sourceKeyPair = KeyPair.random();

    private final KeyPair destinationKeyPair = KeyPair.random();

    private final Network network = new Network("unit-test-network");

    @BeforeEach
    void setUp() {
        stellarExecutor = new StellarExecutor(stellarServerHandler, ioHandler);
    }

    @Test
    void execute_help() {
        // given
        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.HELP);

        stellarExecutor.execute(givenParams);

        // then
        verifyNoInteractions(stellarServerHandler);
        verify(ioHandler).printHelp();
    }

    @Test
    void execute_create_keypair() {
        // given
        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.CREATE_KEYPAIR);

        // when
        stellarExecutor.execute(givenParams);

        // then
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).printf(eq(Constants.TEMPLATE_MESSAGE_PUBLIC_KEY), anyString());
        verify(ioHandler).printf(eq(Constants.TEMPLATE_MESSAGE_PRIVATE_KEY), anyString());
    }

    @Test
    void execute_print_balance_account_not_found() throws IOException {
        // given
        String givenPublicKey = sourceKeyPair.getAccountId();

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.PRINT_BALANCE, givenPublicKey);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenPublicKey)).thenThrow(org.stellar.sdk.requests.ErrorResponse.class);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);


        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_UNABLE_TO_FIND_ACCOUNT, givenPublicKey);
    }

    @Test
    void execute_print_balance_no_balance() throws IOException {
        // given
        String givenPublicKey = sourceKeyPair.getAccountId();

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.PRINT_BALANCE, givenPublicKey);

        AccountResponse.Balance[] givenBalance = new AccountResponse.Balance[0];


        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenPublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getBalances()).thenReturn(givenBalance);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(accountsRequestBuilder).account(givenPublicKey);
        verify(ioHandler).errorPrintln(Constants.ERROR_THERES_NO_BALANCE);
    }

    @Test
    void execute_print_balance_valid_public_key() throws IOException {
        // given
        String givenAssertType = "dsfhgas7d78s";
        String givenBalanceAmount = "dsf7u8hsdf87hsdf7sdfh";
        String givenPublicKey = sourceKeyPair.getAccountId();

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.PRINT_BALANCE, givenPublicKey);
        AccountResponse.Balance[] givenBalance = {accountBalance};

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenPublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getBalances()).thenReturn(givenBalance);
        when(accountBalance.getAssetType()).thenReturn(givenAssertType);
        when(accountBalance.getBalance()).thenReturn(givenBalanceAmount);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(accountsRequestBuilder).account(givenPublicKey);
        verify(ioHandler).printf(Constants.TEMPLATE_BALANCES_ACCOUNT, givenPublicKey);
        verify(ioHandler).printf(Constants.TEMPLATE_BALANCES_TYPE_AMOUNT, givenAssertType, givenBalanceAmount);
        verify(ioHandler).printf(Constants.TEMPLATE_PRINT_BALANCE_SUMMARY, givenPublicKey);
    }

    @Test
    void execute_print_balance_invalid_public_key() {
        // given
        String givenPublicKey = "abcdef";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        givenParams.add(ApplicationParameters.PRINT_BALANCE, givenPublicKey);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_INVALID_PUBLIC_KEY, givenPublicKey);
    }

    @Test
    void execute_send_with_more_than_max_operation_in_trx() throws IOException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);

        for (int i = 0; i < 21; i++) {
            addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);
        }

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenDestinationPublicKey)).thenReturn(destinationAccountResponse);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintln(Constants.ERROR_TRANSACTION_HAS_MORE_OPERATIONS_THAN_MAX);
    }

    @Test
    void execute_send_twenty_trx() throws IOException, AccountRequiresMemoException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePublicKey = sourceKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        for (int i = 0; i < 20; i++) {
            addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);
        }

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenDestinationPublicKey)).thenReturn(destinationAccountResponse);
        when(accountsRequestBuilder.account(givenSourcePublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getAccountId()).thenReturn(givenSourcePublicKey);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarServerHandler).submitTransaction(sendTransactionCaptor.capture());
        Transaction trxToAssert = sendTransactionCaptor.getValue();

        // then
        assertWarningMessageIsPrinted();
        assertThat(trxToAssert.getSourceAccount()).isEqualTo(givenSourcePublicKey);
        assertThat(trxToAssert.getNetwork()).isEqualTo(network);
        assertThat(trxToAssert.getFee()).isEqualTo(AbstractTransaction.MIN_BASE_FEE * 20);
        assertThat(trxToAssert.getMemo()).isInstanceOf(MemoNone.class);

        Operation[] operations = trxToAssert.getOperations();
        assertThat(operations).hasSize(20);

        assertPaymentOperation(operations[0], givenSourcePublicKey, givenDestinationPublicKey, giveAmount);
    }

    @Test
    void execute_send() throws IOException, AccountRequiresMemoException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePublicKey = sourceKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenDestinationPublicKey)).thenReturn(destinationAccountResponse);
        when(accountsRequestBuilder.account(givenSourcePublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getAccountId()).thenReturn(givenSourcePublicKey);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarServerHandler).submitTransaction(sendTransactionCaptor.capture());
        Transaction trxToAssert = sendTransactionCaptor.getValue();

        // then
        assertWarningMessageIsPrinted();
        assertThat(trxToAssert.getSourceAccount()).isEqualTo(givenSourcePublicKey);
        assertThat(trxToAssert.getNetwork()).isEqualTo(network);
        assertThat(trxToAssert.getFee()).isEqualTo(AbstractTransaction.MIN_BASE_FEE);
        assertThat(trxToAssert.getMemo()).isInstanceOf(MemoNone.class);

        Operation[] operations = trxToAssert.getOperations();
        assertThat(operations).hasSize(1);

        assertPaymentOperation(operations[0], givenSourcePublicKey, givenDestinationPublicKey, giveAmount);
    }

    @Test
    void execute_send_user_selected_no_confirmation() throws IOException, AccountRequiresMemoException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.NO);

        stellarExecutor.execute(givenParams);

        // then
        verify(stellarServerHandler, times(0)).submitTransaction(any());
    }

    @Test
    void execute_send_not_enough_in_account() throws IOException, AccountRequiresMemoException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePublicKey = sourceKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "999.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenDestinationPublicKey)).thenReturn(destinationAccountResponse);
        when(accountsRequestBuilder.account(givenSourcePublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getAccountId()).thenReturn(givenSourcePublicKey);
        when(stellarServerHandler.submitTransaction(any())).thenReturn(submitTransactionResponse);
        when(submitTransactionResponse.getExtras()).thenReturn(extras);
        when(extras.getResultCodes()).thenReturn(resultCodes);
        when(resultCodes.getTransactionResultCode()).thenReturn(FAILED.getCode());
        when(resultCodes.getOperationsResultCodes()).thenReturn(new ArrayList<>(Arrays.asList("A", "B", "C")));
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();

        verify(ioHandler).errorPrintln(Constants.ERROR_TRX_FAILED);
        verify(ioHandler).errorPrintf(Constants.ERROR_TEMPLATE_TRC_FAILED, "A");
        verify(ioHandler).errorPrintf(Constants.ERROR_TEMPLATE_TRC_FAILED, "B");
        verify(ioHandler).errorPrintf(Constants.ERROR_TEMPLATE_TRC_FAILED, "C");
    }

    @Test
    void execute_send_create_invalid_source_private_key() {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = "abdjshdg784h";
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_INVALID_PRIVATE_KEY, givenSourcePrivateKey);
    }

    @Test
    void execute_send_invalid_destination_public_key() {
        // given
        String givenDestinationPublicKey = "6ads6asd6d6sd6";
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_INVALID_PUBLIC_KEY, givenDestinationPublicKey);
    }

    @Test
    void execute_send_destination_account_not_present() throws IOException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenDestinationPublicKey)).thenThrow(org.stellar.sdk.requests.ErrorResponse.class);

        stellarExecutor.execute(givenParams);

        // then
        assertWarningMessageIsPrinted();
        verify(stellarServerHandler).getNetwork();
        verify(stellarServerHandler).accounts();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.TEMPLATE_ERROR_UNABLE_TO_FIND_ACCOUNT, givenDestinationPublicKey);
    }

    @Test
    void execute_send_create_amount_more_than_seven_digits() {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "123.45678912";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        // then
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.ERROR_SPECIFIED_AMOUNT, giveAmount);
    }

    @Test
    void execute_send_create_amount_not_a_number() {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "abecd";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addSendParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        stellarExecutor.execute(givenParams);

        // then
        verify(stellarServerHandler).getNetwork();
        verifyNoMoreInteractions(stellarServerHandler);
        verify(ioHandler).errorPrintf(Constants.ERROR_SPECIFIED_AMOUNT, giveAmount);
    }

    @Test
    void execute_create() throws IOException, AccountRequiresMemoException {
        // given
        String givenDestinationPublicKey = destinationKeyPair.getAccountId();
        String givenSourcePublicKey = sourceKeyPair.getAccountId();
        String givenSourcePrivateKey = new String(sourceKeyPair.getSecretSeed());
        String giveAmount = "41.1020551";

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        addSourceParameter(givenParams, givenSourcePrivateKey);
        addCreateAccountParameter(givenParams, givenDestinationPublicKey, giveAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);
        when(accountsRequestBuilder.account(givenSourcePublicKey)).thenReturn(sourceAccountResponse);
        when(sourceAccountResponse.getAccountId()).thenReturn(givenSourcePublicKey);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarServerHandler).submitTransaction(sendTransactionCaptor.capture());
        Transaction trxToAssert = sendTransactionCaptor.getValue();

        // then
        assertWarningMessageIsPrinted();
        assertThat(trxToAssert.getSourceAccount()).isEqualTo(givenSourcePublicKey);
        assertThat(trxToAssert.getNetwork()).isEqualTo(network);
        assertThat(trxToAssert.getFee()).isEqualTo(AbstractTransaction.MIN_BASE_FEE);
        assertThat(trxToAssert.getMemo()).isInstanceOf(MemoNone.class);

        Operation[] operations = trxToAssert.getOperations();
        assertThat(operations).hasSize(1);

        assertAccountCreationOperation(operations[0], givenSourcePublicKey, givenDestinationPublicKey, giveAmount);
    }

    private void assertWarningMessageIsPrinted() {
        verify(ioHandler).errorPrintln(Constants.MSG_WARNING_USES_THIS_CLI_AT_YOUR_OWN_RISK);
        verify(ioHandler).errorPrintln(Constants.MSG_THIS_SOFTWARE_IS_UNDER_THE_APACHE_LICENSE);
        verify(ioHandler).println(Constants.MSG_PRESS_THE_ENTER_KEY_TO_CONTINUE);
    }
}