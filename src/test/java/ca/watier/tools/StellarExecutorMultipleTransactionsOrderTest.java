/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools;

import ca.watier.TransactionAndOperationUtils;
import ca.watier.tools.handlers.IOHandler;
import ca.watier.tools.handlers.StellarServerHandler;
import ca.watier.tools.models.ApplicationParameters;
import ca.watier.tools.models.PrintBalance;
import ca.watier.tools.models.StellarOperation;
import ca.watier.tools.models.YesNoResponse;
import ca.watier.utils.collections.MultiValueCollection;
import ca.watier.utils.collections.MultiValueCollectionImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Network;
import org.stellar.sdk.Operation;
import org.stellar.sdk.requests.AccountsRequestBuilder;
import org.stellar.sdk.responses.AccountResponse;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Tests to check the order of the {@link StellarOperation}, before the conversion to the {@link Operation} / other action
 */
@ExtendWith(MockitoExtension.class)
class StellarExecutorMultipleTransactionsOrderTest {

    @Captor
    private ArgumentCaptor<List<StellarOperation>> stellarOperationCaptor;

    @Mock
    private AccountsRequestBuilder accountsRequestBuilder;

    @Mock
    private StellarServerHandler stellarServerHandler;

    @Mock
    private IOHandler ioHandler;

    private StellarExecutor stellarExecutor;

    private final Network network = new Network("unit-test-network");

    @BeforeEach
    void setUp() {
        stellarExecutor = spy(new StellarExecutor(stellarServerHandler, ioHandler));
    }

    @Test
    void print_payment() {
        // given
        String givenAmount = "123.4567892";

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePrivateKey, givenAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(2);
        assertThat(trxToAssert.get(0)).isInstanceOf(PrintBalance.class);
        assertThat(trxToAssert.get(1)).isInstanceOf(ca.watier.tools.models.Transaction.class);
    }

    @Test
    void print() {
        // given
        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(1);
        assertThat(trxToAssert.get(0)).isInstanceOf(PrintBalance.class);
    }

    @Test
    void print_print() {
        // given
        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(2);
        assertThat(trxToAssert.get(0)).isInstanceOf(PrintBalance.class);
        assertThat(trxToAssert.get(1)).isInstanceOf(PrintBalance.class);
    }

    @Test
    void payment() {
        // given
        String givenAmount = "123.4567892";

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePrivateKey, givenAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(1);
        assertThat(trxToAssert.get(0)).isInstanceOf(ca.watier.tools.models.Transaction.class);
    }

    @Test
    void payment_print() {
        // given
        String givenAmount = "123.4567892";

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePrivateKey, givenAmount);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(2);
        assertThat(trxToAssert.get(0)).isInstanceOf(ca.watier.tools.models.Transaction.class);
        assertThat(trxToAssert.get(1)).isInstanceOf(PrintBalance.class);
    }

    @Test
    void payment_print_print_payment_different_sources() throws IOException {
        // given
        String givenAmount = "123.4567892";

        AccountResponse givenFirstSourceAccountResponse = mock(AccountResponse.class);
        AccountResponse givenSecondSourceAccountResponse = mock(AccountResponse.class);

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());

        KeyPair givenSecondSourceKeyPair = KeyPair.random();
        String givenSecondSourcePublicKey = givenSecondSourceKeyPair.getAccountId();
        String givenSecondSourcePrivateKey = new String(givenSecondSourceKeyPair.getSecretSeed());

        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, givenAmount);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addSourceParameter(givenParams, givenSecondSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, givenAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);

        when(givenFirstSourceAccountResponse.getAccountId()).thenReturn(givenFirstSourcePublicKey);
        when(givenSecondSourceAccountResponse.getAccountId()).thenReturn(givenSecondSourcePublicKey);

        when(accountsRequestBuilder.account(givenFirstSourcePublicKey)).thenReturn(givenFirstSourceAccountResponse);
        when(accountsRequestBuilder.account(givenSecondSourcePublicKey)).thenReturn(givenSecondSourceAccountResponse);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(4);

        StellarOperation firstTrx = trxToAssert.get(0);
        assertThat(firstTrx).isInstanceOf(ca.watier.tools.models.Transaction.class);

        assertThat(trxToAssert.get(1)).isInstanceOf(PrintBalance.class);
        assertThat(trxToAssert.get(2)).isInstanceOf(PrintBalance.class);

        StellarOperation secondTrx = trxToAssert.get(3);
        assertThat(secondTrx).isInstanceOf(ca.watier.tools.models.Transaction.class);
    }

    @Test
    void payment_print_print_payment_same_sources() throws IOException {
        // given
        String givenAmount = "123.4567892";

        AccountResponse givenFirstSourceAccountResponse = mock(AccountResponse.class);

        KeyPair givenFirstSourceKeyPair = KeyPair.random();
        String givenFirstSourcePublicKey = givenFirstSourceKeyPair.getAccountId();
        String givenFirstSourcePrivateKey = new String(givenFirstSourceKeyPair.getSecretSeed());


        MultiValueCollection<ApplicationParameters, String> givenParams = MultiValueCollectionImpl.arrayList();

        TransactionAndOperationUtils.addSourceParameter(givenParams, givenFirstSourcePrivateKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, givenAmount);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addPrint(givenParams, givenFirstSourcePublicKey);
        TransactionAndOperationUtils.addSendParameter(givenParams, givenFirstSourcePublicKey, givenAmount);

        // when
        when(stellarServerHandler.getNetwork()).thenReturn(network);
        when(stellarServerHandler.accounts()).thenReturn(accountsRequestBuilder);

        when(givenFirstSourceAccountResponse.getAccountId()).thenReturn(givenFirstSourcePublicKey);

        when(accountsRequestBuilder.account(givenFirstSourcePublicKey)).thenReturn(givenFirstSourceAccountResponse);
        when(ioHandler.askYesNoQuestion(Constants.CONFIRMATION_EXECUTION_TRX)).thenReturn(YesNoResponse.YES);

        stellarExecutor.execute(givenParams);

        verify(stellarExecutor).printSummaryAndExecuteOperationsIfConfirmed(anyBoolean(), any(Network.class), stellarOperationCaptor.capture());
        List<StellarOperation> trxToAssert = stellarOperationCaptor.getValue();

        // then
        assertThat(trxToAssert).hasSize(3);
        assertThat(trxToAssert.get(0)).isInstanceOf(ca.watier.tools.models.Transaction.class); //2x operations
        assertThat(trxToAssert.get(1)).isInstanceOf(PrintBalance.class);
        assertThat(trxToAssert.get(2)).isInstanceOf(PrintBalance.class);
    }
}